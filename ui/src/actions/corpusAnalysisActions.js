import { endpoint } from 'src/app/api';
import asyncActionCreator from './asyncActionCreator';
import { MAX_RESULTS } from './util';


// export const fetchCorpusAnalysis = asyncActionCreator(() => async () => {
//   const response = await endpoint.get('corpus_analysis', { });
//   return response.data;
// }, { name: 'FETCH_CORPUS_ANALYSIS' });
// export const fetchCorpusAnalysis = asyncActionCreator(() => async () => endpoint.get('/corpus_analysis', {
//   params: {},
// }), { name: 'FETCH_CORPUS_ANALYSIS' });

export const fetchCorpusAnalysis = asyncActionCreator((selectedQueries) => async () => {

  // selectedItems = selectedItems || [''];

  selectedQueries = selectedQueries || [];

  console.log('fetchCorpusAnalysis queries: ', selectedQueries);

  const params = { query: selectedQueries };
  const response = await endpoint.get('corpus_analysis', {params});

  return { data: response.data };
}, { name: 'FETCH_CORPUS_ANALYSIS' });
