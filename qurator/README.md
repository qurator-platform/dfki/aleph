# Qurator Aleph

### Getting started

```bash
# Change to this directory
cd qurator

# Build all images (optional) 
make build

# Pull pre-build images
make docker-pull

# Run the latest database migrations and create/update the search index.
make fast-upgrade

# Run the web-based API server and the user interface.
make web
```

Open http://localhost:8080/ in your browser to visit the web frontend.

### Working in a shell

```bash
make shell
# ...

# This will result in a root shell inside the container:
aleph --help

# Create new user account
aleph createuser --name="Alice" \
                 --admin \
                 --password=123abc \
                 user@example.com
```

## Worker
```bash
make worker
```