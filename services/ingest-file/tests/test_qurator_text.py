# -*- coding: utf-8 -*-
import logging

from normality.cleaning import decompose_nfkd
from followthemoney.proxy import EntityProxy
from followthemoney import model
from followthemoney.types import registry


from ingestors.analysis import Analyzer

from .support import TestCase

log = logging.getLogger(__name__)

class QuratorTextIngestorTest(TestCase):

    # def test_match(self):
    #     fixture_path, entity = self.fixture('utf.txt')
    #     self.manager.ingest(fixture_path, entity)
    #
    #     self.assertTrue(isinstance(entity, EntityProxy))
    #     self.assertEqual(entity.first('mimeType'), 'text/plain')
    #
    #     self.assertEqual(decompose_nfkd(entity.first('bodyText')),
    #                      decompose_nfkd(u'Îș unî©ođ€.'))
    #     self.assertEqual(
    #         entity.first('processingStatus'), self.manager.STATUS_SUCCESS
    #     )
    #     self.assertEqual(entity.schema, 'PlainText')
    #
    # def test_ingest_binary_mode(self):
    #     fixture_path, entity = self.fixture('non_utf.txt')
    #     self.manager.ingest(fixture_path, entity)
    #
    #     self.assertIn(u'größter', entity.first('bodyText'))
    #     self.assertEqual(entity.schema, 'PlainText')
    #
    def test_ingest_extra_fixture(self):
        fixture_path, entity = self.fixture('udhr_ger.txt')
        self.manager.ingest(fixture_path, entity)

        log.error(entity)

        self.assertIsNotNone(entity.first('bodyText'))
        self.assertEqual(entity.schema, 'PlainText')

        self.assertEqual(0, 1, 'QURATOR')

    def _tagged_entity(self, entity):
        a = Analyzer(self.manager.dataset, entity)
        a.feed(entity)
        a.flush()
        return self.get_emitted_by_id(entity.id)

    def test_ner_extract(self):
        text = 'Das ist der Pudel von Angela Merkel. '
        text = text * 5
        entity = model.make_entity('PlainText')
        entity.id = 'test1'
        entity.add('bodyText', text)
        entity = self._tagged_entity(entity)
        names = entity.get_type_values(registry.name)
        
        assert 'Angela Merkel' in names, names

        log.info(f'{entity}')
        log.info(f'{dir(entity)}')

    # def test_always_fail(self):
    #     log.info('ffoooo')
    #     self.assertEqual(0, 1, 'QURATOR')
