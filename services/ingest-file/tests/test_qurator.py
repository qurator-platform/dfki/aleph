# -*- coding: utf-8 -*-
import logging

from followthemoney import model

from ingestors.qurator import TopicDetectionService
from .support import TestCase

log = logging.getLogger(__name__)


class QuratorTest(TestCase):
    def test_topic_classification(self):
        log.debug('xx')

        text = 'Das ist der Pudel von Angela Merkel. '
        text = text * 5
        entity = model.make_entity('PlainText')
        entity.id = 'test1'
        entity.add('bodyText', text)

        # Send to service
        srv = TopicDetectionService(dataset=self.manager.dataset, fragment='qurator')
        srv.process(entity)

        topics = srv.get_topics_from_text(text)

        log.info(f'Extracted topics: {topics}')
