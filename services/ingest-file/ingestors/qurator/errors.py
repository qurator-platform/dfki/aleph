from ingestors.exc import ProcessingException


class QuratorError(ProcessingException):
    pass
