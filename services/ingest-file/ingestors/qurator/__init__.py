import logging
from typing import List

from ingestors.manager import Manager
from ingestors.qurator.services import QuratorService
from ingestors.qurator.services.echo import EchoService
from ingestors.qurator.services.topic_detection import TopicDetectionService

log = logging.getLogger(__name__)

default_services = [
    EchoService,
    # TopicDetectionService
]


class Qurator(object):
    fragment = 'qurator'
    services = None  # type: List[QuratorService]

    def __init__(self, task):
        self.entity_ids = task.payload.get('entity_ids')
        self.dataset = Manager.get_dataset(task.stage, task.context)

        # pipeline = task.context.get('pipeline')
        # languages = task.context.get('languages')
        # balkhash_name = task.context.get('balkhash_name')

    def get_services(self):
        if not self.services:
            # Use default services
            self.services = []
            for cls in default_services:
                srv = cls(dataset=self.dataset, fragment=self.fragment)

                if srv.is_available():
                    self.services.append(srv)
                else:
                    log.warning(f'Service is not available: {cls}')

        return self.services

    def process_dataset(self):
        # Iterate over entities in dataset
        for entity in self.dataset.partials(entity_id=self.entity_ids):
            for srv in self.get_services():
                # Process entity with each service
                srv.process(entity)

        return self.entity_ids
