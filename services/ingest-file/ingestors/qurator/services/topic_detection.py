import logging
from typing import List

import requests
from followthemoney.types import registry
from servicelayer import env

from ingestors.qurator import QuratorService
from ingestors.qurator.errors import QuratorError

log = logging.getLogger(__name__)


class TopicDetectionService(QuratorService):
    field = 'annotatedTopics'

    api_base_url = env.get('QURATOR_TOPIC_DETECTION_API', 'http://0.0.0.0:7000/srv/text-classification')
    model_id = env.get('QURATOR_TOPIC_DETECTION_MODEL', 'spacy_dewikinews')
    classification_threshold = float(env.get('QURATOR_TOPIC_DETECTION_THRESHOLD', 0.01))

    def is_available(self):
        # Send a simple ping to check whether service is available
        try:
            res = requests.get(self.api_base_url)
            
            if res.status_code == 200:
                return True
        except BaseException:
            pass
        
        return False

    def get_topics_from_text(self, text: str) -> List[str]:

        # API endpoint
        req_url = self.api_base_url + f'/models/{self.model_id}/classify'
        res = requests.post(req_url, data=dict(text=text, classification_threshold=self.classification_threshold))

        if res.status_code == 200:
            try:
                res = res.json()

                log.debug(f'API response: {res}')

                # topics are predictions of classifier
                return res['prediction']

            except ValueError as e:
                raise QuratorError(f'Cannot parse service API response: {e}')
        else:
            raise QuratorError(f'Service API responds with code={res.status_code}; Request URL: {req_url}')

    def process(self, entity):
        if not entity.schema.is_a(self.entity_schema):
            return

        log.info(f'Topic detection for: {entity}')

        texts = entity.get_type_values(registry.text)

        # join text chunks to single text
        text = '\n'.join(texts)

        # actual processing
        topics = self.get_topics_from_text(text)
        # topics = ['Politics', 'Sports']

        for val in topics:
            entity.add(self.field, val)

        self.dataset.put(entity, self.fragment)
