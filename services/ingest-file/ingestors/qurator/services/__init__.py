
class QuratorService(object):
    entity_schema = 'Annotatable'

    def __init__(self, dataset, fragment):
        self.dataset = dataset
        self.fragment = fragment

    def is_available(self):
        raise NotImplementedError()

    def process(self, entity):
        raise NotImplementedError()
