import logging
from followthemoney.types import registry

from ingestors.qurator import QuratorService

log = logging.getLogger(__name__)


class EchoService(QuratorService):
    """
    Dummy service that only logs entity text chunks
    """
    entity_schema = 'Annotatable'

    def process(self, entity):
        texts = entity.get_type_values(registry.text)

        for t in texts:
            log.info(f'Entity text: {t}')
            
    def is_available(self):
        return True  # always available!
